public class Part1 {
    public static int maxProduct(Array a, int m){
        //Implementing a sliding window solution
        //first we take care of base cases
        if(a.length() == 0){
            return 1;
        }
        if(a.length() == 1){
            return a.getVal(0);
        }
        if(a.length()<m){
            int product = 1;
            for(int i=0;i<a.length();i++){
                product = product*a.getVal(i);
            }
            return product;
        }
        int j=0;
        //move all zeroes to the end
        for (int i = 0; i < a.length(); i++) {
            if (a.getVal(i) != 0) {
                a.swap(j, i); 
                j++;
            }
        }
        System.out.println(a.toString());
        int product = 1;
        
        // preparing a m-sized window and saving the product in a variable
        for(int i=0;i<m;i++){
            
            product *= a.getVal(i);
        }
        int maxProduct = Integer.MIN_VALUE;
        for(int i=m;i<a.length();i++){
            //changing max product if its less than product
            if(maxProduct<product){
                maxProduct = product;
            }
           if(a.getVal(i) == 0)
           {
            break;
           }
            //the following lines are for shifting the window accordingly
            product = product/a.getVal(i-m);
            product = product*a.getVal(i);
            
        }
        //checking the last variable if it computes to max product
        if(maxProduct<product){
            maxProduct = product;
        }
        return maxProduct;
    }
    public static void main(String args[]){
        Array x = new Array(10);
        for(int i=1;i<6;i++){
            x.setVal(i, i);
        }
        x.setVal(6,1);
        System.out.println(x.toString());
        System.out.println(maxProduct(x, 3));

    }
}
