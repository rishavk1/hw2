import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;
import java.util.List;
//updated with public methods instead of default method
@SuppressWarnings("unchecked")
public class Graph{
    
    private Map<Integer, ArrayList<Integer> > map = new HashMap<Integer,ArrayList<Integer>>();
    private Integer matrix[][];
    private ArrayList<ArrayList<Integer>> SSC;
    private Integer numberOfVertices;
    private Integer numberOfEdges;
    private boolean isDirected;
    private int time;


    /*
     * constructor; n = the number of vertices; directed = true if the graph is directed, false if it is undirected
     */
    
    public Graph(int n, boolean directed){
        numberOfVertices = n;
        isDirected = directed;
        matrix = new Integer[n][n];
        numberOfEdges = 0;
    }
    /*
     * constructor; fn = the filename containing the graph information (see below for a breakdown of the files); 
     * NOTE: Do not hard-code any path names or filenames or your code may not work with the autograder!
     */
    public Graph(String fn){
        File file = new File(fn);
        Scanner sc;
        try {
            sc = new Scanner(file);
        } catch (FileNotFoundException e) {
            return;
        }
        String isDir = sc.next();
        
       
        int n = Integer.parseInt(sc.next());

        boolean isDirected;
        List<String> vertices = new ArrayList<String>();
        while(sc.hasNext()){
            String x = sc.next();
            String y = sc.next();
            vertices.add(x+" "+y);
            
        }
        for(int i=0;i<vertices.size();i++){
            for(int j=i+1;j<vertices.size();j++){
                if(vertices.get(i).equals(vertices.get(j)) || vertices.get(i).equals(new StringBuilder(vertices.get(j)).reverse().toString()))
                {
                    vertices.remove(j);
                    j--;
                }
            }
        }
        if(isDir.equals("u")){
            isDirected = false;
        }
        else
        {
            isDirected = true;
        }
        int numberOfEdges = 0;
        Integer[][] matrix = new Integer[n][n];
        for(String i: vertices){
            Integer source = Integer.parseInt(i.split(" ")[0]);
            Integer dest = Integer.parseInt(i.split(" ")[1]);
            matrix[source][dest] = 1;
            
            if(map.containsKey(source)){
                ArrayList<Integer> x = map.get(source);
                x.add(dest);
            }
            if(!map.containsKey(source)){
                ArrayList<Integer> x = new ArrayList<Integer>();
                x.add(dest);
                map.put(source,x);
               
            }
            if(!isDirected){
                matrix[dest][source] = 1;
                if(map.containsKey(dest)){
                    ArrayList<Integer> x = map.get(dest);
                    x.add(source);
                }
                if(!map.containsKey(dest)){
                    ArrayList<Integer> x = new ArrayList<Integer>();
                    x.add(source);
                    map.put(dest,x);
                }
            }
        }
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                if(matrix[i][j] != null){
                    numberOfEdges++;
                }
            }
    
        }
        if(!isDirected){
            numberOfEdges/=2;
        }
        this.matrix = matrix;
        this.numberOfVertices = n;
        this.isDirected = isDirected;
        this.numberOfEdges = numberOfEdges;
        sc.close();
    }

    /*
     * return the number of vertices
     */

    public int V(){
        return numberOfVertices;
    }
    /*
     * return the number of edges
     */

    public int E(){
        return numberOfEdges;
    }
    /*
     * add an edge from v to w unless (v, w) already exists in the graph
     */

    public void addEdge(int v, int w){
       
            if(matrix[v][w] == null){
                matrix[v][w] = 1;
                this.numberOfEdges++;
                if(map.containsKey(v)){
                    ArrayList<Integer> x = map.get(v);
                    x.add(w);
                }
                else{
                    ArrayList<Integer> x = new ArrayList<Integer>();
                    x.add(w);
                    map.put(v,x);
                }
            }
        if(!isDirected){
            if(matrix[w][v] == null){
                matrix[w][v] = 1;
                if(map.containsKey(w)){
                    ArrayList<Integer> x = map.get(w);
                    x.add(v);
                }
                else{
                    ArrayList<Integer> x = new ArrayList<Integer>();
                    x.add(v);
                    map.put(w,x);
                }
            }
        }
        
    }
    /*
     * return true iff v is adjacent to w
     */
    public boolean adjTo(int v, int w){
        if(matrix[v][w] == null){
            return false;
        }
        return true;
    }
    /*
     * return true iff the graph is simple
     */
    public boolean isSimple(){
        for(int i=0;i<V();i++){
            ArrayList<Integer> x = map.get(i);
            if(x != null){
                //Check for self-loop
                if(x.contains(i)){
                    return false;
                }
            }
        }
        // no checking for parallel edges required since we are not allowing them
        return true;
    }


    private void DFS_components(int i, boolean[] visited, ArrayList<Integer> conMap){
        visited[i] = true;
        if(map.get(i) == null){
            return;
        }
        for (int x : map.get(i)) {
            if (!visited[x]){
                conMap.add(x);
                DFS_components(x, visited, conMap);
        }
    }
    }
    private void SSC_DFS(int i, int low[],int disc[], boolean stackMem[], Stack<Integer> stack){
        disc[i] = time;
        low[i] = time;
        time+=1;
        stackMem[i] = true;
        stack.push(i);
        ArrayList<Integer> x =  map.get(i);
        if(x != null){
          
        for(int num:x){
            if(disc[num] == -1){
                SSC_DFS(num, low, disc, stackMem, stack);
                low[i] = Math.min(low[i],low[num]);
            }
            else if(stackMem[num] == true){
                low[i] = Math.min(low[i],disc[num]);
            }
        }
    }
        int w = -1;
        ArrayList<Integer> newarr = new ArrayList<Integer>();
        if(low[i] == disc[i]){
            while(w!=i){
                w = (int)stack.pop();
                newarr.add(w);
                stackMem[w] = false;
            }
            SSC.add(newarr);
        }
    }
    /*
     *  return a Hashmap containing lists that
        indicate the connected components (for an
        undirected graph) or strongly connected
        components (for a directed graph); the key for
        each component should be the smallest vertex
        in that component
     */
    public HashMap<Integer,ArrayList<Integer>> components(){
        if(!isDirected){
        boolean visited[] = new boolean[numberOfVertices];
        HashMap<Integer, ArrayList<Integer>> connectedMap = new HashMap<Integer, ArrayList<Integer>>();
        for(int i = 0; i < numberOfVertices;i++){
            if(!visited[i]){
                ArrayList<Integer> conMap = new ArrayList<Integer>();
                conMap.add(i);
                DFS_components(i, visited, conMap);
                connectedMap.put(i,conMap);
            }
        }
        return connectedMap;
    }
    else{
        int disc[] = new int[V()];
        int low[] = new int[V()];
        for (int i = 0; i < V(); i++) {
            disc[i] = -1;
            low[i] = -1;
        }
        boolean stackMem[] = new boolean[V()];
        Stack<Integer> stack = new Stack<Integer>();
        SSC = new ArrayList<ArrayList<Integer>>();
        for(int i=0;i<V();i++){
            if(disc[i] == -1){
                SSC_DFS(i,low,disc,stackMem,stack);
            }
        }
        HashMap<Integer,ArrayList<Integer>> returnVal = new HashMap<Integer,ArrayList<Integer>>();
        for(ArrayList<Integer> x: SSC){
            int min = x.get(0);
            for(int now: x){
                min = Math.min(min, now);
            }
            returnVal.put(min,x);
        }
        return returnVal;
    }
    }


    /*
     *  return the number of connected (or strongly
        connected for digraphs) components in the
        graph
     */
    public int numComponents(){
        return components().size();
    }
    /*
     * return true iff the graph (only undirected) is
       biconnected
     */
    public boolean isBiconnected(){
        if(articulationVertices().size() == 0){
            return true;
        }

        return false;
    }

    public void articulationVerticesHelper(int i, boolean visited[], int disc[],int low[],int parent, boolean isAP[]){
        int child = 0;

        visited[i] = true;

        disc[i] = low[i] = ++time;

        for(Integer v: map.get(i)){

            if(!visited[v]){
                ++child;
                articulationVerticesHelper(v, visited, disc, low, i, isAP);
                low[i] = Math.min(low[i],low[v]);

                if(parent != -1 && low[v] >= disc[i]){
                    isAP[i] = true;
                }
            }
            else if(v != parent){
                low[i] = Math.min(low[i], disc[v]);
            }
        }

        if(parent == -1 && child> 1){
            isAP[i] = true;
        }
    }
    /*
     * return a list of the articulation vertices in
     * the graph (only undirected graphs)
     */
    public ArrayList<Integer> articulationVertices(){

        boolean[] visited = new boolean[V()];
        int[] disc = new int[V()];
        int[] low = new int[V()];
        boolean[] isAP = new boolean[V()];
        int time = 0; 
        int par = -1;

        for(int i=0;i<V();i++){
            if(visited[i] == false){
                articulationVerticesHelper(i, visited, disc, low, par, isAP);
            }
        }

        ArrayList<Integer> returnValue= new ArrayList<Integer>();
        for(int i=0;i<V();i++){
            if(isAP[i] == true){
                returnValue.add(i);
            }
        }


        return returnValue;
    }
    /*
     * return true iff the graph is acyclic
     */
    public boolean isAcyclic(){
        for(int i=0;i<V();i++){
            if(cycle(i) != null){
                return false;
            }
        }
        return true;
    }

    public void DFS_path(int u, int v, boolean visited[], ArrayList<Integer> currPath,ArrayList<ArrayList<Integer>> foundPath, boolean findAllPath){
        if(!findAllPath && foundPath.size() == 1){
            return;
        }
        if(visited[u] == true){
            return;
        }
        visited[u] = true;
        currPath.add(u);
        if(u == v){
            foundPath.add((ArrayList<Integer>)currPath.clone());
            visited[u] = false;
            currPath.remove(currPath.size()-1);
            return;
        }
        if(!map.containsKey(u))
        {
            return;
        }
        for(int i: map.get(u)){
            DFS_path(i,v, visited, currPath, foundPath,findAllPath);
        }
        currPath.remove(currPath.size()-1);
        visited[u] = false;
    }

    /*
     * return a list of vertices that make up a path from v to w; return null if such a path does not exist
     */
    public ArrayList<Integer> path(int v, int w){
        boolean visited[] = new boolean[V()];
        ArrayList<Integer> currPath = new ArrayList<Integer>();
        ArrayList<ArrayList<Integer>> foundPath = new ArrayList<ArrayList<Integer>>();
        DFS_path(v ,w, visited, currPath,foundPath, false);
       if(foundPath.size() == 0){
        return null;
       }
        return foundPath.get(0);
    }

    /*
     * return a list of vertices that make up a cycle starting and ending at v; return null if such a cycle does not exist; a self-loop may count as a cycle
     */
    public ArrayList<Integer> cycle(int v){
       ArrayList<Integer> x = map.get(v);
        boolean visited[] = new boolean[V()];
        ArrayList<Integer> currPath = new ArrayList<Integer>();
        ArrayList<ArrayList<Integer>> foundPath = new ArrayList<ArrayList<Integer>>();
        for(int w: x)
        DFS_path(w ,v, visited, currPath,foundPath, true);
        if(foundPath.size() == 0){
            return null;
        }        
        for(ArrayList<Integer> paths: foundPath){
            if(paths.size() > 2){
                paths.add(0,v);
                return paths;
            }
        }
        return null;
    }
    /*
     * return a graph that is a spanning tree of this graph (undirected graphs only)
     */
    public Graph spanningTree(){
        return null;
    }
    public void DFS_isConnected(int source, boolean visited[]){
        visited[source] = true;
        ArrayList<Integer> list = map.get(source);
        if(list == null){
            return;
        }
        for(int i=0;i<list.size();i++){
            int neighbor = list.get(i);
            if(visited[neighbor] == false)
            DFS_isConnected(neighbor, visited);
        }
    }
    /*
     * return true if the graph is connected (for undirected graphs)
     */
    public boolean isConnected(){
        
        boolean visited[] = new boolean[V()];
        DFS_isConnected(0, visited);
        int num_visited = 0;
        for(boolean x:visited){
            if(x){
                num_visited++;
            }
        }
    
        if(V() == num_visited){
            return true;
        }
        return false;
    }
    /*
     * return true if the graph is strongly connected (for directed graphs)
     */
    public boolean isStronglyConnected(){
        return components().size() ==1;
    }
    /*
     * return a list of vertices that is a
        topological ordering of the (directed) graph;
        return null if such an ordering does not exist
     */
    public void topSort_DFS(int i, boolean visited[], Stack<Integer> stack){
        visited[i] = true;

        ArrayList<Integer> x = map.get(i);
        if(x != null){
            for(int n:x){
                if(!visited[n]){
                    topSort_DFS(n, visited, stack);
                }
            }
        }
        stack.push(i);
    }
    public ArrayList<Integer> topSort(){
        if(!isAcyclic())
        return null;
        
        boolean visited[] = new boolean[V()];
        Stack<Integer> stack = new Stack<Integer>();
        for(int i=0;i<V();i++){
            if(visited[i] == false){
                topSort_DFS(i, visited,stack);
            }
        }
        ArrayList<Integer> returnVal = new ArrayList<Integer>();
        while(stack.empty() == false){
            returnVal.add(stack.pop());
        }
        return returnVal;
    }


    /*
     * return true iff v is an articulation vertex (undirected graphs only
     */
    public boolean isArticulation(int v){
        boolean[] visited = new boolean[V()];
        int[] disc = new int[V()];
        int[] low = new int[V()];
        boolean[] isAP = new boolean[V()];
        int time = 0; 
        int par = -1;

        articulationVerticesHelper(v, visited, disc, low, par, isAP);

        return isAP[v];
    }
    /*
     * return true iff (v, w) is a bridge (which is
        an edge that is like an articulation vertex–if
        it is removed, the graph becomes disconnected
     */
    public boolean isBridge(int v, int w){
        HashMap<Integer,ArrayList<Integer>> copy = new HashMap<Integer,ArrayList<Integer>>();
        for(Map.Entry<Integer,ArrayList<Integer>> entry: map.entrySet()){
            copy.put(entry.getKey(),entry.getValue());
        }
        if(map.get(v).size()>1){
            ArrayList<Integer> x = map.get(v);
            x.remove(x.indexOf(w));
        }
        if(map.get(v).size() == 1){
            map.remove(v);
        }
        if(!isDirected){
            if(map.get(w).size()>1){
                ArrayList<Integer> x = map.get(w);
                x.remove(x.indexOf(v));
            }
            if(map.get(w).size() == 1){
                map.remove(w);
            }
        }
        boolean returnValue = !(numComponents() == 1);
        map = copy; 
       
        return returnValue;
    }
    public void dfs_order_helper(int i, boolean visited[], ArrayList<Integer> returnVal){
        visited[i] = true;
        returnVal.add(i);
        ArrayList<Integer> x = map.get(i);
        if(x != null){
            for(int num: x){
                if(!visited[num]){
                    dfs_order_helper(num, visited, returnVal);
                }
            }
        }
    }
    /*
     * return a list of the vertices in the order they are discovered when doing dfs starting at vertex v
     */
    public ArrayList<Integer> dfsOrder(int v){
        ArrayList<Integer> returnVal = new ArrayList<Integer>();

        boolean visited[] = new boolean[V()];

        dfs_order_helper(v, visited, returnVal);
        return returnVal;
    }

    
    /*
     * return a list of the vertices in the order they are discovered when doing bfs starting at vertex v
     */
    public ArrayList<Integer> bfsOrder(int v){
        boolean visited[] = new boolean[V()];
        ArrayList<Integer> x = new ArrayList<Integer>();
        LinkedList<Integer> queue = new LinkedList<Integer>();
        visited[v] = true;
        queue.add(v);

        while(queue.size()!=0){
            int i = queue.poll();
            x.add(i);
            ArrayList<Integer> here = map.get(i);
            if(here!= null){
                for(int num:here){
                    if(!visited[num]){
                        visited[num] = true;
                        queue.add(num);
                    }
                }
            }
        }
        return x;
    }

    /*
     * return true if the graphs are the same–meaning
        that they have the same vertices and edges,
        but the order of the edges in the
        representation does not matter
     */
    public boolean equals(Graph G){
        if(G.V() != V()){
            return false;
        }
        for(int i=0;i<V();i++){
            for(int j=0;j<V();j++){
                if(G.adjTo(i, j) != adjTo(i,j)){
                
                    return false;
                }
            }
        }
        return true;
    }

    /*
     * return a graph that is the transitive closure of this graph (directed graphs only)
     */
    public Graph transitiveClosure(){
        Graph G = new Graph(V(),true);
        for(int i=0;i<V();i++){
            for(int j=0;j<V();j++){
                if(path(i,j) != null){
                    if(i != j)
                    G.addEdge(i,j);
                   
                }
            }
        }
        return G;
    }
    /*
     * return the degree of the vertex (for
       undirected graphs)
     */
    public int degree(int v){
        ArrayList<Integer> x = map.get(v);
        if(x == null){
            return 0;
        }
        else{
            return x.size();
        }
    }
    /*
     * return the indegree of the vertex (for directed graphs)
     */
    public int indegree(int v){
        int indegree = 0;
        for(int i=0;i<V();i++){
            if(matrix[i][v] != null){
                indegree++;
            }
        }
        return indegree;
    }
    /*
     * return the outdegree of the vertex (for directed graphs)
     */
    public int outdegree(int v){
        ArrayList<Integer> x = map.get(v);
        if(x == null){
            return 0;
        }
        else{
            return x.size();
        }
    }
}
