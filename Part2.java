public class Part2 {
    public static void pushZeroes(Array a){
        int j=0;
        //iterate over array
        for(int i=0;i<a.length();i++){
            if(a.getVal(i) != 0){
                //swap array position i and j if it is zero
                int temp = a.getVal(i);
                a.setVal(i,a.getVal(j));
                a.setVal(j, temp);
                j+=1;
            }
        }
    }

}
